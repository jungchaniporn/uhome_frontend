import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import DeviceStatus from '@/components/DeviceStatus'
import Setup from '@/components/Setup'
import AddDevices from '@/components/AddDevices'
import Routine from '@/components/Routine'
import Loading from '@/components/Loading'
import ForceAddHome from '@/components/ForceAddHome'
import AddResident from '@/components/AddResident'
import Home from '@/components/Home'
import EachHome from '@/components/EachHome'
import EachRoom from '@/components/EachRoom'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '*',
      name: 'Login',
      component: Login
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/DeviceStatus',
      name: 'DeviceStatus',
      component: DeviceStatus,
      props: true,
      meta:{
        requiresAuth:true
      }
    },
    {
      path: '/Setup',
      name: 'Setup',
      component: Setup,
      props: true,
      meta:{
        requiresAuth:true
      }
    },
    {
      path: '/AddDevices',
      name: 'AddDevices',
      component: AddDevices,
      props: true,
      meta:{
        requiresAuth:true
      }
    },
    {
      path: '/Routine',
      name: 'Routine',
      component: Routine,
      props: true,
      meta:{
        requiresAuth:true
      }
    },
    {
      path: '/Loading',
      name: 'Loading',
      component: Loading,
      props: true,
      meta:{
        requiresAuth:true
      }
    },
    {
      path: '/ForceAddHome/:HomeID',
      name: 'ForceAddHome',
      component: ForceAddHome,
      props: true,
      meta:{
        requiresAuth:true
      }
    },
    {
      path: '/AddResident/:HomeID',
      name: 'AddResident',
      component: AddResident,
      props: true,
      meta:{
        requiresAuth:true
      }
    },
    {
      path: '/Home',
      name: 'Home',
      component: Home,
      props: true,
      meta:{
        requiresAuth:true
      }
    },
    {
      path: '/EachHome/:HomeID/:HomeName',
      name: 'EachHome',
      component: EachHome,
      props: true,
      meta:{
        requiresAuth:true
      }
    },
    {
      path: '/EachRoom/:RoomID/:RoomName',
      name: 'EachRoom',
      component: EachRoom,
      props: true,
      meta:{
        requiresAuth:true
      }
    }
  ]
})

router.beforeEach((to,from,next)=>{
  let currentUser = firebase.auth().currentUser
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  console.log(currentUser,requiresAuth)
  if(currentUser){
    console.log("currently login")
    if(requiresAuth){
      console.log("page require authen")
      next()
    }else{
      console.log("page no auth, can't go")
      next({path:'/Login'})
    }
  }
  else{
    console.log("not login")
    if(to.path==="/Login"){
      next()
    }
    else{
      next('Login')
    }
  }
})
export default router;