// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'vuetify/dist/vuetify.min.css'
import vSelect from 'vue-select'
import firebase from 'firebase/app'
import 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/database'
import firebaseui from 'firebaseui'

let app;
Vue.config.productionTip = false
// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyBWUtbxKBnglyCxYFAAbMpeyQNuSt6Hgp8",
  authDomain: "uhome-bd34e.firebaseapp.com",
  databaseURL: "https://uhome-bd34e.firebaseio.com",
  projectId: "uhome-bd34e",
  storageBucket: "uhome-bd34e.appspot.com",
  messagingSenderId: "514062511683",
  appId: "1:514062511683:web:b5f42933a08e60b401aef6",
  measurementId: "G-JFF1W2HXSX",

  clientId: "115066901153-lkc46liobl117k61nqv921q649he12g7.apps.googleusercontent.com",

  scopes: [
    "email",
    "profile"
  ]
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
window.firebase = firebase;

/* eslint-disable no-new */
firebase.auth().onAuthStateChanged((user)=>{
  // Vue.component('v-select', vSelect)
  Vue.use(Vuetify)
  if(!app){
    new Vue({
      el: '#app',
      router,
      components: { App },
      template: '<App/>'
    })
  }  
  
})

